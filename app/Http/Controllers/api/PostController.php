<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Http\Resources\PostResource;
use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            'message' => 'Display a listing of the posts',
            'posts' => Post::all()
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|string',
            'body' => 'required|string',
            'image' => 'required|string',
            'user_id' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Error',

            ], 422);
        }

        $post = Post::create([
            'title' => $request->title,
            'body' => $request->body,
            'image' => $request->image,
            'user_id' => $request->user_id
        ]);

        if (isset($post)) {
            return response()->json([
                'message' => 'post store successfully.',
                'data' => $post
            ], 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);
        if (isset($post)) {
            return response()->json([
                'message' => "Display the post $id",
                'posts' => new PostResource($post)
            ], 200);
        } else {
            return response()->json([
                'message' => "not found"

            ], 404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'string',
            'body' => 'string',
            'image' => 'string',
            'user_id' => 'integer'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Error',

            ], 422);
        }

        $post = Post::find($id);

        if (isset($post)) {
            $update = $post->update([
                'title' => $request->title ?? $post->title,
                'body' => $request->body ?? $post->body,
                'image' => $request->image ?? $post->image,
                'user_id' => $request->user_id ?? $post->user_id
            ]);

            return response([
                'message' => "update the post $id successfully"
            ], 200);
        } else {
            return response([
                'message' => "not found"
            ], 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);
        if (isset($post)) {
            $post->delete();
            return response([
                'message' => "delete the post $id successfully"
            ], 200);
        }
    }
}
